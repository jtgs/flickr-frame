import flickrapi
from flask import Flask, render_template
import datetime as dt
import random

FLICKR_KEY = "[[redacted]]"
FLICKR_SECRET = "[[redacted]]"

# My '365' album
ALBUM_ID = "72157688594867732"

WEEKDAYS = ["MON", "TUE", "WED", "THU", "FRI", "SAT", "SUN"]
MONTHS = ["-", "JAN", "FEB", "MAR", "APR", "MAY", "JUN",
               "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]

photos_list = []


class Photo:
    def __init__(self, url, datestr):
        self.url = url
        date_obj = dt.datetime.strptime(datestr, "%Y-%m-%d %H:%M:%S").date()
        self.day = WEEKDAYS[date_obj.weekday()]
        self.date = date_obj.day
        self.mon = MONTHS[date_obj.month]


def make_url(photo):
    # url_template = "http://farm{farm-id}.staticflickr.com/
    #                 {server-id}/{id}_{secret}_[mstzb].jpg"
    photo['filename'] = "%(id)s_%(secret)s_z.jpg" % photo
    url = ("http://farm%(farm)s.staticflickr.com/%(server)s/%(filename)s"
           % photo)
    return url

def get_photos():
    flickr = flickrapi.FlickrAPI(FLICKR_KEY, FLICKR_SECRET)
    photos = flickr.walk_set(ALBUM_ID)
    photos_list = []
    for photo in photos:
        url = make_url(photo.attrib)
        info = flickr.photos.getInfo(photo_id=photo.attrib['id'])
        date_taken = info.find('photo').find('dates').attrib['taken']
        photos_list.append(Photo(url, date_taken))
    random.shuffle(photos_list)
    return photos_list

app = Flask(__name__)

@app.route('/')
def photoframe():
    return render_template('template.html', photos=photos_list)

if __name__ == '__main__':
    photos_list = get_photos()
    app.run()
